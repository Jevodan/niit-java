package jevo;

import java.util.Map;
import java.util.Set;

/**
 * Created by Alexander on 16.04.2017.
 */
public abstract class Engineer extends Employee implements WorkTime,Project {
    public int workTimePay() {return this.getHours() * this.getRate();}
    public int projectPay(){
        int numberProjects=0 ;
        for(String pro : proj){
            if (this.getProjects().indexOf(pro)>0)
                numberProjects++;
        }
        return numberProjects * BASE_PROJECT;}
    public int buh(){return (int)(workTimePay() + projectPay());}
}

class Programmer extends  Engineer{
        public Programmer(){
            this.setRate(600);
        }

}

class Tester extends Engineer{
        public Tester(){
            this.setRate(450);
        }

}

class TeamLeader extends Programmer implements Healing{
        public TeamLeader(){
            this.setRate(1100);
        }

        public int buh(){return (int)(workTimePay() + projectPay() + healingPay());}

}
