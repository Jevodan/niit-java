/**
 * Created by Alexander on 15.03.2017.
 */
public class Program {

    public static void main(String[] args)
    {
        double zazor; // зазор
        double costDor;
        double costOgr;
        double itog;

             // Расчет зазора задачка 1
        Circle earth = new Circle();
        earth.setRadius(6378.1*1000);
        Circle space = new Circle();
        space.setFerence(earth.getFerence() + 1);
        zazor = space.getRadius() - earth.getRadius();
        System.out.println("Зазор равен: " + zazor + " метров");
            // Расчет стоимости ограды и дорожки задачка 2

        Circle full = new Circle();
        Circle bass = new Circle();
        bass.setRadius(3);
        full.setRadius(bass.getRadius() + 1);
        costDor = (full.getArea() - bass.getArea())*1000;
        costOgr = bass.getFerence()*2000;
        itog = costDor + costOgr;
        System.out.println("Стоимость дорожки:  " + costDor);
        System.out.println("Стоимость ограды: " + costOgr);
        System.out.println("Всего: " + itog);
    }
}
