package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static java.lang.Integer.parseInt;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Кофейный автомат");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        Controller cont = new Controller();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
